/* Generated By:JavaCC: Do not edit this line. RobotParser.java */
package uniandes.lym.robot.control;

import uniandes.lym.robot.kernel.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;



@SuppressWarnings("serial")
public class RobotParser implements RobotParserConstants {


        private RobotWorldDec robotw;

        private ArrayList <String > vars = new ArrayList<String>();
        private ArrayList <Integer> varsVal = new ArrayList<Integer>();


        void setWorld(RobotWorld w) {
                robotw = (RobotWorldDec) w;
        }

    void delay(int d) {
                                try {
                                        Thread.sleep(d);
                                    } catch (InterruptedException e) {
                                        System.err.format("IOException: %s%n", e);
                                    }

    }

  final public boolean instructions(StringBuffer system) throws ParseException {
                                String output=new String();
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case ROBOT_R:
      jj_consume_token(ROBOT_R);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case VARS:
        jj_consume_token(VARS);
        vars();
        break;
      default:
        jj_la1[0] = jj_gen;
        ;
      }
      block();
      break;
    case T_MOVE:
    case T_TURNRIGHT:
    case T_PUTB:
    case T_PICKB:
    case T_PUTC:
    case T_PICKC:
      label_1:
      while (true) {
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case T_TURNRIGHT:
          jj_consume_token(T_TURNRIGHT);
                                    robotw.turnRight();
          break;
        case T_MOVE:
          jj_consume_token(T_MOVE);
                                   robotw.moveForward(1) ;
          break;
        case T_PUTB:
          jj_consume_token(T_PUTB);
                              robotw.putBalloons(1);
          break;
        case T_PICKB:
          jj_consume_token(T_PICKB);
                               robotw.grabBalloons(1) ;
          break;
        case T_PUTC:
          jj_consume_token(T_PUTC);
                              robotw.putChips(1);
          break;
        case T_PICKC:
          jj_consume_token(T_PICKC);
                               robotw.pickChips(1) ;
          break;
        default:
          jj_la1[1] = jj_gen;
          jj_consume_token(-1);
          throw new ParseException();
        }
        jj_consume_token(47);
                  delay(1000);
        switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
        case T_MOVE:
        case T_TURNRIGHT:
        case T_PUTB:
        case T_PICKB:
        case T_PUTC:
        case T_PICKC:
          ;
          break;
        default:
          jj_la1[2] = jj_gen;
          break label_1;
        }
      }
      jj_consume_token(0);
                   {if (true) return true;}
      break;
    default:
      jj_la1[3] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  final public void move() throws ParseException {
          Token t;
                int tem;
    jj_consume_token(T_MOVE);
    jj_consume_token(LB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NUMBER:
      t = jj_consume_token(NUMBER);
                          robotw.moveForward(Integer.parseInt(t.image));
      break;
    case NAME:
      t = jj_consume_token(NAME);
                                                                                        tem = getVal(t); robotw.moveForward(tem);
      break;
    default:
      jj_la1[4] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case RB:
      jj_consume_token(RB);
      break;
    case COMA:
      jj_consume_token(COMA);
      move2(t);
      break;
    default:
      jj_la1[5] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void face() throws ParseException {
    jj_consume_token(FACE);
    jj_consume_token(LB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NORTH:
      jj_consume_token(NORTH);
    while(robotw.getOrientation()!=0)
                        {
                                robotw.turnRight();
                        }
      break;
    case SOUTH:
      jj_consume_token(SOUTH);
    while(robotw.getOrientation()!=1)
                        {
                                robotw.turnRight();
                        }
      break;
    case EAST:
      jj_consume_token(EAST);
    while(robotw.getOrientation()!=2)
                        {
                                robotw.turnRight();
                        }
      break;
    case WEST:
      jj_consume_token(WEST);
    while(robotw.getOrientation()!=3)
                        {
                                robotw.turnRight();
                        }
      break;
    default:
      jj_la1[6] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RB);
  }

  final public void turn() throws ParseException {
         Token t;
    jj_consume_token(TURN);
    jj_consume_token(LB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case RIGHT:
      t = jj_consume_token(RIGHT);
                        robotw.turnRight();
      break;
    case LEFT:
      t = jj_consume_token(LEFT);
                    robotw.turnRight();robotw.turnRight(); robotw.turnRight();
      break;
    case ARROUND:
      t = jj_consume_token(ARROUND);
                          robotw.turnRight();robotw.turnRight(); robotw.turnRight();robotw.turnRight();
      break;
    default:
      jj_la1[7] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RB);
  }

  final public void put() throws ParseException {
         Token n;
         int i=1;
         int tem;
    jj_consume_token(PUT);
    jj_consume_token(LB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case CHIPS:
      jj_consume_token(CHIPS);
      jj_consume_token(COMA);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NUMBER:
        n = jj_consume_token(NUMBER);
                                                  tem =Integer.parseInt(n.image);
        break;
      case NAME:
        n = jj_consume_token(NAME);
                                                                                               tem=getVal(n);
        break;
      default:
        jj_la1[8] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
                    while (i<=tem)
                        {
                                robotw.putChip();
                                i++;
                          }
      break;
    case BALLOONS:
      jj_consume_token(BALLOONS);
      jj_consume_token(COMA);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NUMBER:
        n = jj_consume_token(NUMBER);
                                                          tem =Integer.parseInt(n.image);
        break;
      case NAME:
        n = jj_consume_token(NAME);
                                                                                                       tem=getVal(n);
        break;
      default:
        jj_la1[9] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
                     while (i<=tem)
                         {
                                robotw.putBalloon();
                                i++;
                          }
      break;
    default:
      jj_la1[10] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RB);
  }

  final public void pick() throws ParseException {
         Token n;
         int i=1;
    jj_consume_token(PICK);
    jj_consume_token(LB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case CHIPS:
      jj_consume_token(CHIPS);
      jj_consume_token(COMA);
      n = jj_consume_token(NUMBER);
                    while (i<=Integer.parseInt(n.image))
                        {
                                robotw.pickChip();
                                i++;
                          }
      break;
    case BALLOONS:
      jj_consume_token(BALLOONS);
      jj_consume_token(COMA);
      n = jj_consume_token(NUMBER);
                     while (i<=Integer.parseInt(n.image))
                         {
                                robotw.pickBalloon();
                                i++;
                          }
      break;
    default:
      jj_la1[11] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RB);
  }

  final public void moveDir() throws ParseException {
Token n;
int result=0;
    jj_consume_token(MOVEDIR);
    jj_consume_token(LB);
    n = jj_consume_token(NUMBER);
result= Integer.parseInt(n.image);
    jj_consume_token(COMA);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FRONT:
      jj_consume_token(FRONT);
robotw.moveForward(result);
      break;
    case RIGHT:
      jj_consume_token(RIGHT);
                                robotw.turnRight();
                                robotw.moveForward(result);
                                robotw.turnRight();
                                robotw.turnRight();
                                robotw.turnRight();
      break;
    case LEFT:
      jj_consume_token(LEFT);
                                robotw.turnRight();
                                robotw.turnRight();
                                robotw.turnRight();
                                robotw.moveForward(result);
                                robotw.turnRight();
      break;
    case BACK:
      jj_consume_token(BACK);
                                robotw.turnRight();
                                robotw.turnRight();
                                robotw.moveForward(result);
                                robotw.turnRight();
                                robotw.turnRight();
      break;
    default:
      jj_la1[12] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RB);
  }

  final public void move2(Token n) throws ParseException {
int result=0;
result= Integer.parseInt(n.image);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NORTH:
      jj_consume_token(NORTH);
     while(robotw.getOrientation()!=0)
                        {
                                robotw.turnRight();
                        }
                        robotw.moveForward(result);
      break;
    case SOUTH:
      jj_consume_token(SOUTH);
    while(robotw.getOrientation()!=1)
                        {
                                robotw.turnRight();

                        }
                        robotw.moveForward(result);
      break;
    case EAST:
      jj_consume_token(EAST);
    while(robotw.getOrientation()!=2)
                        {
                                robotw.turnRight();
                        }
                        robotw.moveForward(result);
      break;
    case WEST:
      jj_consume_token(WEST);
    while(robotw.getOrientation()!=3)
                        {
                                robotw.turnRight();
                        }
                        robotw.moveForward(result);
      break;
    default:
      jj_la1[13] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(RB);
  }

  final public boolean facing() throws ParseException {
  boolean rta=false;
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FACING:
      jj_consume_token(FACING);
      jj_consume_token(RB);
      jj_consume_token(NORTH);
    if(robotw.getOrientation()==0)
                {
                                rta=true;
                        }
      break;
    case SOUTH:
      jj_consume_token(SOUTH);
   if(robotw.getOrientation()==1)
                {
                                rta=true;
                        }
      break;
    case EAST:
      jj_consume_token(EAST);
   if(robotw.getOrientation()==2)
                {
                                rta=true;
                        }
      break;
    case WEST:
      jj_consume_token(WEST);
    if(robotw.getOrientation()==3)
                {
                                rta=true;
                        }
      jj_consume_token(LB);
{if (true) return rta;}
      break;
    default:
      jj_la1[14] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  final public boolean canMove() throws ParseException {
  boolean rta = true;
    jj_consume_token(CANMOVE);
    jj_consume_token(RB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case NORTH:
      jj_consume_token(NORTH);
    if(robotw.atTop() ==true)
                {
                                rta=true;
                        }
      break;
    case SOUTH:
      jj_consume_token(SOUTH);
   if(robotw.atBottom()==true)
                {
                                rta=true;
                        }
      break;
    case EAST:
      jj_consume_token(EAST);
   if(robotw.atRight()==true)
                {
                                rta=true;
                        }
      break;
    case WEST:
      jj_consume_token(WEST);
    if(robotw.atLeft()==true)
                {
                                rta=true;
                        }
      break;
    default:
      jj_la1[15] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(LB);
{if (true) return rta;}
    throw new Error("Missing return statement in function");
  }

  final public boolean canPick() throws ParseException {
  boolean rta=true;
  int x=0;
  Token n;
    jj_consume_token(CANPICK);
    jj_consume_token(RB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case CHIPS:
      jj_consume_token(CHIPS);
    if(robotw.anyChips()==false)
    {
      rta=false;
    }
    else {
      x=robotw.getMyChips();
    }
      break;
    case BALLOONS:
      jj_consume_token(BALLOONS);
  if(robotw.anyBalloons()==false)
    {
      rta=false;
    }
      else
       {
      x=robotw.countBalloons();
    }
      break;
    default:
      jj_la1[16] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(COMA);
    n = jj_consume_token(NUMBER);
  if(x<Integer.parseInt(n.image))
  {
    rta=false;
}
    jj_consume_token(LB);
    {if (true) return rta;}
    throw new Error("Missing return statement in function");
  }

  final public boolean canPut() throws ParseException {
  boolean rta=true;
  int x=0;
  Token n;
    jj_consume_token(CANPUT);
    jj_consume_token(RB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case CHIPS:
      jj_consume_token(CHIPS);
    if(robotw.getMyChips()==0)
    {
      rta=false;
    }
    else {
      x=robotw.getMyChips();
    }
      break;
    case BALLOONS:
      jj_consume_token(BALLOONS);
  if(robotw.getMyBalloons()==0)
    {
      rta=false;
    }
      else
       {
      x=robotw.getMyBalloons();
    }
      break;
    default:
      jj_la1[17] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(COMA);
    n = jj_consume_token(NUMBER);
  if(x<Integer.parseInt(n.image))
  {
    rta=false;
}
    jj_consume_token(LB);
    {if (true) return rta;}
    throw new Error("Missing return statement in function");
  }

  final public boolean not() throws ParseException {
  boolean rta=true;
    jj_consume_token(NOT);
    jj_consume_token(RB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FACING:
      jj_consume_token(FACING);
      jj_consume_token(RB);
      if(facing()==true)
      {
        rta=false;
      }
      jj_consume_token(LB);
      break;
    case CANMOVE:
      jj_consume_token(CANMOVE);
      jj_consume_token(RB);
      if(canMove()==true)
      {
        rta=false;
      }
      jj_consume_token(LB);
      break;
    case CANPICK:
      jj_consume_token(CANPICK);
      jj_consume_token(RB);
      if(canPick()==true)
      {
        rta=false;
      }
      jj_consume_token(LB);
      break;
    case CANPUT:
      jj_consume_token(CANPUT);
      jj_consume_token(RB);
      if(canPut()==true)
      {
        rta=false;
      }
      jj_consume_token(LB);
      break;
    default:
      jj_la1[18] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(LB);
{if (true) return rta;}
    throw new Error("Missing return statement in function");
  }

  final public void vars() throws ParseException {
         Token t;
    label_2:
    while (true) {
      t = jj_consume_token(NAME);
            vars.add(t.image);
                varsVal.add(0);
      jj_consume_token(COMA);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case NAME:
        ;
        break;
      default:
        jj_la1[19] = jj_gen;
        break label_2;
      }
    }
  }

  final public void assign() throws ParseException {
        Token val;
        Token  t;
        int n;
    jj_consume_token(ASSIGN);
    jj_consume_token(LB);
    t = jj_consume_token(NAME);
    jj_consume_token(COMA);
    val = jj_consume_token(NUMBER);
             n=vars.indexOf(t.image);
             if(n!= -1)
             System.out.println(varsVal.get(n));
              varsVal.set(n, Integer.parseInt(val.image));
    jj_consume_token(RB);
  }

  final public void block() throws ParseException {
    jj_consume_token(BEGIN);
    label_3:
    while (true) {
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case ASSIGN:
        assign();
        break;
      case T_MOVE:
        move();
        break;
      case TURN:
        turn();
        break;
      case FACE:
        face();
        break;
      case PUT:
        put();
        break;
      case PICK:
        pick();
        break;
      case MOVEDIR:
        moveDir();
        break;
      case SKIP_C:
        jj_consume_token(SKIP_C);
        break;
      default:
        jj_la1[20] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      jj_consume_token(47);
                  delay(1000);
      switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
      case T_MOVE:
      case ASSIGN:
      case TURN:
      case FACE:
      case PUT:
      case PICK:
      case MOVEDIR:
      case SKIP_C:
        ;
        break;
      default:
        jj_la1[21] = jj_gen;
        break label_3;
      }
    }
    jj_consume_token(END);
  }

  final public void if_m() throws ParseException {
          boolean rta=false;
    jj_consume_token(IF);
    jj_consume_token(RB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FACING:
      jj_consume_token(FACING);
      if(facing()==true)
      {
        rta=true;
      }
      break;
    case CANMOVE:
      jj_consume_token(CANMOVE);
      if(canMove()==true)
      {
        rta=true;
      }
      break;
    case CANPICK:
      jj_consume_token(CANPICK);
      if(canPick()==true)
      {
        rta=true;
      }
      break;
    case CANPUT:
      jj_consume_token(CANPUT);
      if(canPut()==true)
      {
        rta=true;
      }
      break;
    case NOT:
      jj_consume_token(NOT);
if(not()==true)
{
rta=true;
}
      break;
    default:
      jj_la1[22] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(COMA);
    jj_consume_token(BLOCK);
if(rta==true)
{
block();
}
    jj_consume_token(COMA);
  block();
    jj_consume_token(LB);
  }

  final public void while_m() throws ParseException {
  boolean rta=true;
    jj_consume_token(WHILE);
    jj_consume_token(RB);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FACING:
      jj_consume_token(FACING);
      if(facing()==true)
      {
        rta=true;
      }
      break;
    case CANMOVE:
      jj_consume_token(CANMOVE);
      if(canMove()==true)
      {
        rta=true;
      }
      break;
    case CANPICK:
      jj_consume_token(CANPICK);
      if(canPick()==true)
      {
        rta=true;
      }
      break;
    case CANPUT:
      jj_consume_token(CANPUT);
      if(canPut()==true)
      {
        rta=true;
      }
      break;
    case NOT:
      jj_consume_token(NOT);
if(not()==true)
{
rta=true;
}
      break;
    default:
      jj_la1[23] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(COMA);
    jj_consume_token(BLOCK);
    while(rta==true)
    {
      block();
    }
    jj_consume_token(LB);
  }

  final public void repeat() throws ParseException {
  int cont=0;
  Token n;
  int result;
    jj_consume_token(REPEAT);
    jj_consume_token(RB);
    n = jj_consume_token(NUMBER);
        result= Integer.parseInt(n.image);
    jj_consume_token(COMA);
    switch ((jj_ntk==-1)?jj_ntk():jj_ntk) {
    case FACING:
      jj_consume_token(FACING);
      while(cont <result)
      {
        facing();
        cont++;
      }
      break;
    case CANMOVE:
      jj_consume_token(CANMOVE);
      while(cont <result)
      {
        canMove();
        cont++;
      }
      break;
    case CANPICK:
      jj_consume_token(CANPICK);
      while(cont <result)
      {
        canPick();
        cont++;
      }
      break;
    case CANPUT:
      jj_consume_token(CANPUT);
      while(cont <result)
      {
        canPut();
        cont++;
      }
      break;
    case NOT:
      jj_consume_token(NOT);
      while(cont <result)
      {
        not();
        cont++;
      }
      break;
    default:
      jj_la1[24] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    jj_consume_token(LB);
  }

  final public int getVal(Token t) throws ParseException {
        int i ;
  i=vars.indexOf(t.image);
  if (i!=-1)
  {
    {if (true) return varsVal.get(i);}
  }
  else
  {
    {if (true) return 0;} }
    throw new Error("Missing return statement in function");
  }

  /** Generated Token Manager. */
  public RobotParserTokenManager token_source;
  SimpleCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private int jj_gen;
  final private int[] jj_la1 = new int[25];
  static private int[] jj_la1_0;
  static private int[] jj_la1_1;
  static {
      jj_la1_init_0();
      jj_la1_init_1();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x4000,0x7e0,0x7e0,0xfe0,0x0,0x1400000,0x0,0x70000000,0x0,0x0,0x6000000,0x6000000,0xb8000000,0x0,0x0,0x0,0x6000000,0x6000000,0x0,0x0,0x3f8020,0x3f8020,0x0,0x0,0x0,};
   }
   private static void jj_la1_init_1() {
      jj_la1_1 = new int[] {0x0,0x0,0x0,0x0,0x6000,0x0,0xf,0x0,0x6000,0x6000,0x0,0x0,0x0,0xf,0x10e,0xf,0x0,0x0,0x170,0x4000,0x0,0x0,0x1f0,0x1f0,0x1f0,};
   }

  /** Constructor with InputStream. */
  public RobotParser(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public RobotParser(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new RobotParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 25; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 25; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public RobotParser(java.io.Reader stream) {
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new RobotParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 25; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 25; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public RobotParser(RobotParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 25; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(RobotParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 25; i++) jj_la1[i] = -1;
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[48];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 25; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
          if ((jj_la1_1[i] & (1<<j)) != 0) {
            la1tokens[32+j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 48; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

}
