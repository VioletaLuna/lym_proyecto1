/* Generated By:JavaCC: Do not edit this line. RobotParserConstants.java */
package uniandes.lym.robot.control;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface RobotParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int T_MOVE = 5;
  /** RegularExpression Id. */
  int T_TURNRIGHT = 6;
  /** RegularExpression Id. */
  int T_PUTB = 7;
  /** RegularExpression Id. */
  int T_PICKB = 8;
  /** RegularExpression Id. */
  int T_PUTC = 9;
  /** RegularExpression Id. */
  int T_PICKC = 10;
  /** RegularExpression Id. */
  int ROBOT_R = 11;
  /** RegularExpression Id. */
  int BEGIN = 12;
  /** RegularExpression Id. */
  int END = 13;
  /** RegularExpression Id. */
  int VARS = 14;
  /** RegularExpression Id. */
  int ASSIGN = 15;
  /** RegularExpression Id. */
  int TURN = 16;
  /** RegularExpression Id. */
  int FACE = 17;
  /** RegularExpression Id. */
  int PUT = 18;
  /** RegularExpression Id. */
  int PICK = 19;
  /** RegularExpression Id. */
  int MOVEDIR = 20;
  /** RegularExpression Id. */
  int SKIP_C = 21;
  /** RegularExpression Id. */
  int RB = 22;
  /** RegularExpression Id. */
  int LB = 23;
  /** RegularExpression Id. */
  int COMA = 24;
  /** RegularExpression Id. */
  int CHIPS = 25;
  /** RegularExpression Id. */
  int BALLOONS = 26;
  /** RegularExpression Id. */
  int FRONT = 27;
  /** RegularExpression Id. */
  int RIGHT = 28;
  /** RegularExpression Id. */
  int LEFT = 29;
  /** RegularExpression Id. */
  int ARROUND = 30;
  /** RegularExpression Id. */
  int BACK = 31;
  /** RegularExpression Id. */
  int NORTH = 32;
  /** RegularExpression Id. */
  int SOUTH = 33;
  /** RegularExpression Id. */
  int WEST = 34;
  /** RegularExpression Id. */
  int EAST = 35;
  /** RegularExpression Id. */
  int CANMOVE = 36;
  /** RegularExpression Id. */
  int CANPICK = 37;
  /** RegularExpression Id. */
  int CANPUT = 38;
  /** RegularExpression Id. */
  int NOT = 39;
  /** RegularExpression Id. */
  int FACING = 40;
  /** RegularExpression Id. */
  int IF = 41;
  /** RegularExpression Id. */
  int WHILE = 42;
  /** RegularExpression Id. */
  int REPEAT = 43;
  /** RegularExpression Id. */
  int BLOCK = 44;
  /** RegularExpression Id. */
  int NUMBER = 45;
  /** RegularExpression Id. */
  int NAME = 46;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\r\"",
    "\"\\t\"",
    "\"\\n\"",
    "\"Move\"",
    "\"TURNRIGHT\"",
    "\"PutB\"",
    "\"PickB\"",
    "\"PutC\"",
    "\"PickC\"",
    "\"ROBOT_R\"",
    "\"BEGIN\"",
    "\"END\"",
    "\"VARS\"",
    "\"Assign\"",
    "\"Turn\"",
    "\"Face\"",
    "\"Put\"",
    "\"Pick\"",
    "\"MoveDir\"",
    "\"Skip\"",
    "\")\"",
    "\"(\"",
    "\",\"",
    "\"Chips\"",
    "\"Balloons\"",
    "\"front\"",
    "\"right\"",
    "\"left\"",
    "\"arround\"",
    "\"back\"",
    "\"north\"",
    "\"south\"",
    "\"west\"",
    "\"east\"",
    "\"canMove\"",
    "\"canPick\"",
    "\"canPut\"",
    "\"not\"",
    "\"facing\"",
    "\"if\"",
    "\"while\"",
    "\"Repeat\"",
    "\"block\"",
    "<NUMBER>",
    "<NAME>",
    "\";\"",
  };

}
